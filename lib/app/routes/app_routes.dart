part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const ROOM = _Paths.ROOM;
  static const CREATEROOM = _Paths.CREATEROOM;
  static const QUESTIONS = _Paths.QUESTIONS;
  static const CREATE_QUESTION = _Paths.CREATE_QUESTION;
  static const COMMENTS = _Paths.COMMENTS;
  static const CREATE_COMMENT = _Paths.CREATE_COMMENT;
  static const LEADERBOARD = _Paths.LEADERBOARD;
  static const GLOBAL_LEADERBOARD = _Paths.GLOBAL_LEADERBOARD;
  static const MY_QUESTIONS = _Paths.MY_QUESTIONS;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const ROOM = '/room';
  static const CREATEROOM = '/create_room';
  static const QUESTIONS = '/questions';
  static const CREATE_QUESTION = '/create-question';
  static const COMMENTS = '/comments';
  static const CREATE_COMMENT = '/create-comment';
  static const LEADERBOARD = '/leaderboard';
  static const GLOBAL_LEADERBOARD = '/global-leaderboard';
  static const MY_QUESTIONS = '/my-questions';
}
